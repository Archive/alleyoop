/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*  Alleyoop
 *  Copyright (C) 2003-2009 Jeffrey Stedfast
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __VG_DEFAULT_VIEW_H__
#define __VG_DEFAULT_VIEW_H__

#include <gtk/gtk.h>

#include <gconf/gconf-client.h>

#include <sys/types.h>

#include "vgtoolview.h"
#include "vgerror.h"

#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus */

#define VG_TYPE_DEFAULT_VIEW            (vg_default_view_get_type ())
#define VG_DEFAULT_VIEW(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), VG_TYPE_DEFAULT_VIEW, VgDefaultView))
#define VG_DEFAULT_VIEW_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), VG_TYPE_DEFAULT_VIEW, VgDefaultViewClass))
#define VG_IS_DEFAULT_VIEW(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), VG_TYPE_DEFAULT_VIEW))
#define VG_IS_DEFAULT_VIEW_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), VG_TYPE_DEFAULT_VIEW))
#define VG_DEFAULT_VIEW_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), VG_TYPE_DEFAULT_VIEW, VgDefaultViewClass))

typedef struct _VgDefaultView VgDefaultView;
typedef struct _VgDefaultViewClass VgDefaultViewClass;

struct _VgDefaultView {
	VgToolView parent_object;
	
	GConfClient *gconf;
	
	GtkWidget *table;
	GtkWidget *rule_list;
	
	GPtrArray *errors;
	VgErrorParser *parser;
	
	GPtrArray *suppressions;
	
	GRegex *search_regex;
	int search_id;
	
	guint rules_id;
	
	int srclines;
	guint lines_id;
};

struct _VgDefaultViewClass {
	VgToolViewClass parent_class;
	
};


GType vg_default_view_get_type (void);

GtkWidget *vg_default_view_new (void);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __VG_DEFAULT_VIEW_H__ */
