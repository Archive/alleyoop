/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*  Alleyoop
 *  Copyright (C) 2003-2009 Jeffrey Stedfast
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __PARSER_H__
#define __PARSER_H__

#include <sys/types.h>

#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus */

#define PARSER_SCAN_HEAD   (128)
#define PARSER_SCAN_BUF    (4096)

typedef struct _Parser {
	unsigned char realbuf[PARSER_SCAN_HEAD + PARSER_SCAN_BUF + 1];
	unsigned char *inbuf;
	unsigned char *inptr;
	unsigned char *inend;
	
	int fd;
} Parser;


void parser_init (Parser *parser, int fd);

ssize_t parser_fill (Parser *parser);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __PARSER_H__ */
