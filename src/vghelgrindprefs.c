/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*  Alleyoop
 *  Copyright (C) 2003-2011 Jeffrey Stedfast
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licenses/>.
 */


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>
#include <stdlib.h>

#include <glib.h>
#include <glib/gi18n.h>
#include <gconf/gconf-client.h>

#include "vghelgrindprefs.h"


#define HAPPENS_BEFORE_KEY    "/apps/alleyoop/valgrind/helgrind/happens-before"
#define ENABLE_TRACE_ADDR_KEY "/apps/alleyoop/valgrind/helgrind/enable-trace-addr"
#define TRACE_ADDR_KEY        "/apps/alleyoop/valgrind/helgrind/trace-addr"
#define TRACE_LEVEL_KEY       "/apps/alleyoop/valgrind/helgrind/trace-level"

static void vg_helgrind_prefs_class_init (VgHelgrindPrefsClass *klass);
static void vg_helgrind_prefs_init (VgHelgrindPrefs *prefs);
static void vg_helgrind_prefs_destroy (GtkObject *obj);
static void vg_helgrind_prefs_finalize (GObject *obj);

static void helgrind_prefs_apply (VgToolPrefs *prefs);
static void helgrind_prefs_get_argv (VgToolPrefs *prefs, const char *tool, GPtrArray *argv);


static VgToolPrefsClass *parent_class = NULL;


GType
vg_helgrind_prefs_get_type (void)
{
	static GType type = 0;
	
	if (!type) {
		static const GTypeInfo info = {
			sizeof (VgHelgrindPrefsClass),
			NULL, /* base_class_init */
			NULL, /* base_class_finalize */
			(GClassInitFunc) vg_helgrind_prefs_class_init,
			NULL, /* class_finalize */
			NULL, /* class_data */
			sizeof (VgHelgrindPrefs),
			0,    /* n_preallocs */
			(GInstanceInitFunc) vg_helgrind_prefs_init,
		};
		
		type = g_type_register_static (VG_TYPE_TOOL_PREFS, "VgHelgrindPrefs", &info, 0);
	}
	
	return type;
}

static void
vg_helgrind_prefs_class_init (VgHelgrindPrefsClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);
	GtkObjectClass *gtk_object_class = GTK_OBJECT_CLASS (klass);
	VgToolPrefsClass *tool_class = VG_TOOL_PREFS_CLASS (klass);
	
	parent_class = g_type_class_ref (VG_TYPE_TOOL_PREFS);
	
	object_class->finalize = vg_helgrind_prefs_finalize;
	gtk_object_class->destroy = vg_helgrind_prefs_destroy;
	
	/* virtual methods */
	tool_class->apply = helgrind_prefs_apply;
	tool_class->get_argv = helgrind_prefs_get_argv;
}


static void
enable_trace_addr_toggled (GtkToggleButton *toggle, VgHelgrindPrefs *prefs)
{
	GConfClient *gconf;
	gboolean bool;
	
	gconf = gconf_client_get_default ();
	
	bool = gtk_toggle_button_get_active (toggle);
	gconf_client_set_bool (gconf, ENABLE_TRACE_ADDR_KEY, bool, NULL);
	gtk_widget_set_sensitive ((GtkWidget *) prefs->trace_level, bool);
	gtk_widget_set_sensitive ((GtkWidget *) prefs->trace_addr, bool);
	
	g_object_unref (gconf);
}

static char *
decode_hex_value (const char *text)
{
	unsigned long int retval;
	char *inend;
	
	retval = strtoul (text, &inend, 16);
	if (inend == text)
		return NULL;
	
	return g_strdup_printf ("0x%lX", retval);
}

static gboolean
entry_focus_out (GtkEntry *entry, GdkEventFocus *event, const char *key)
{
	GConfClient *gconf;
	const char *text;
	char *str;
	
	gconf = gconf_client_get_default ();
	
	if ((text = gtk_entry_get_text (entry))) {
		str = decode_hex_value (text);
	} else {
		str = NULL;
	}
	
	if (str != NULL) {
		gconf_client_set_string (gconf, key, str, NULL);
		gtk_entry_set_text (entry, str);
		g_free (str);
	} else {
		gconf_client_unset (gconf, key, NULL);
		gtk_entry_set_text (entry, "");
	}
	
	g_object_unref (gconf);
	
	return FALSE;
}

static void
menu_item_activated (GtkMenuItem *item, const char *key)
{
	GConfClient *gconf;
	const char *str;
	int val;
	
	gconf = gconf_client_get_default ();
	
	str = g_object_get_data ((GObject *) item, "value");
	
	if (!strcmp (key, TRACE_LEVEL_KEY)) {
		val = strtol (str, NULL, 10);
		gconf_client_set_int (gconf, key, val, NULL);
	} else {
		gconf_client_set_string (gconf, key, str, NULL);
	}
	
	g_object_unref (gconf);
}

static const char *happens_before_opts[] = { "none", "threads", "all" };
static const char *trace_level_opts[] = { "0", "1", "2" };

static GtkWidget *
option_menu_new (GConfClient *gconf, const char **opts, int nopts, const char *key)
{
	GtkWidget *omenu, *menu, *item;
	int history, i;
	char *str;
	
	if (!strcmp (key, TRACE_LEVEL_KEY)) {
		history = gconf_client_get_int (gconf, key, NULL);
		str = NULL;
	} else {
		str = gconf_client_get_string (gconf, key, NULL);
		history = 0;
	}
	
	menu = gtk_menu_new ();
	for (i = 0; i < nopts; i++) {
		if (str && !strcmp (opts[i], str))
			history = i;
		
		item = gtk_menu_item_new_with_label (opts[i]);
		g_object_set_data ((GObject *) item, "value", (char *) opts[i]);
		g_signal_connect (item, "activate", G_CALLBACK (menu_item_activated), (char *) key);
		gtk_widget_show (item);
		
		gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);
	}
	
	gtk_widget_show (menu);
	omenu = gtk_option_menu_new ();
	gtk_option_menu_set_menu ((GtkOptionMenu *) omenu, menu);
	gtk_option_menu_set_history ((GtkOptionMenu *) omenu, history);
	
	g_free (str);
	
	return omenu;
}

static void
vg_helgrind_prefs_init (VgHelgrindPrefs *prefs)
{
	gboolean enable_trace_addr;
	GtkWidget *widget, *hbox;
	GConfClient *gconf;
	char *str;
	
	gconf = gconf_client_get_default ();
	
	((VgToolPrefs *) prefs)->label = _("Helgrind");
	
	gtk_box_set_spacing ((GtkBox *) prefs, 6);
	
	/* --happens-before */
	hbox = gtk_hbox_new (FALSE, 6);
	widget = gtk_label_new (_("Consider sync points that happen before:"));
	gtk_widget_show (widget);
	gtk_box_pack_start ((GtkBox *) hbox, widget, FALSE, FALSE, 0);
	widget = option_menu_new (gconf, happens_before_opts, G_N_ELEMENTS (happens_before_opts), HAPPENS_BEFORE_KEY);
	prefs->happens_before = (GtkOptionMenu *) widget;
	gtk_widget_show (widget);
	gtk_box_pack_start ((GtkBox *) hbox, widget, FALSE, FALSE, 0);
	gtk_widget_show (hbox);
	gtk_box_pack_start ((GtkBox *) prefs, hbox, FALSE, FALSE, 0);
	
	/* --trace-addr */
	hbox = gtk_hbox_new (FALSE, 6);
	widget = gtk_check_button_new_with_label (_("Trace address:"));
	enable_trace_addr = gconf_client_get_bool (gconf, ENABLE_TRACE_ADDR_KEY, NULL);
	gtk_toggle_button_set_active ((GtkToggleButton *) widget, enable_trace_addr);
	g_signal_connect (widget, "toggled", G_CALLBACK (enable_trace_addr_toggled), prefs);
	prefs->enable_trace_addr = (GtkToggleButton *) widget;
	gtk_widget_show (widget);
	gtk_box_pack_start ((GtkBox *) hbox, widget, FALSE, FALSE, 0);
	str = gconf_client_get_string (gconf, TRACE_ADDR_KEY, NULL);
	widget = gtk_entry_new ();
	gtk_entry_set_text ((GtkEntry *) widget, str ? str : "");
	g_signal_connect (widget, "focus-out-event", G_CALLBACK (entry_focus_out), TRACE_ADDR_KEY);
	prefs->trace_addr = (GtkEntry *) widget;
	gtk_widget_show (widget);
	gtk_box_pack_start ((GtkBox *) hbox, widget, FALSE, FALSE, 0);
	gtk_widget_set_sensitive (widget, enable_trace_addr);
	gtk_widget_show (hbox);
	gtk_box_pack_start ((GtkBox *) prefs, hbox, FALSE, FALSE, 0);
	g_free (str);
	
	/* --trace-level */
	hbox = gtk_hbox_new (FALSE, 6);
	widget = gtk_label_new (_("Trace level:"));
	gtk_widget_show (widget);
	gtk_box_pack_start ((GtkBox *) hbox, widget, FALSE, FALSE, 0);
	widget = option_menu_new (gconf, trace_level_opts, G_N_ELEMENTS (trace_level_opts), TRACE_LEVEL_KEY);
	prefs->trace_level = (GtkOptionMenu *) widget;
	gtk_widget_show (widget);
	gtk_box_pack_start ((GtkBox *) hbox, widget, FALSE, FALSE, 0);
	gtk_widget_show (hbox);
	gtk_box_pack_start ((GtkBox *) prefs, hbox, FALSE, FALSE, 0);
	gtk_widget_set_sensitive (widget, enable_trace_addr);
	
	g_object_unref (gconf);
}

static void
vg_helgrind_prefs_finalize (GObject *obj)
{
	G_OBJECT_CLASS (parent_class)->finalize (obj);
}

static void
vg_helgrind_prefs_destroy (GtkObject *obj)
{
	GTK_OBJECT_CLASS (parent_class)->destroy (obj);
}


static void
helgrind_prefs_apply (VgToolPrefs *prefs)
{
	;
}

enum {
	ARG_TYPE_INT,
	ARG_TYPE_STRING
};

static struct {
	const char *key;
	const char *arg;
	gboolean depends;
	char *buf;
	int type;
} helgrind_args[] = {
	{ HAPPENS_BEFORE_KEY, "--happens-before", FALSE, NULL, ARG_TYPE_STRING },
	{ TRACE_ADDR_KEY,     "--trace-addr",     TRUE,  NULL, ARG_TYPE_STRING },
	{ TRACE_LEVEL_KEY,    "--trace-level",    TRUE,  NULL, ARG_TYPE_INT    },
};

static void
helgrind_prefs_get_argv (VgToolPrefs *prefs, const char *tool, GPtrArray *argv)
{
	gboolean enable_trace_addr;
	GConfClient *gconf;
	char *str;
	int val;
	int i;
	
	gconf = gconf_client_get_default ();
	
	enable_trace_addr = gconf_client_get_bool (gconf, ENABLE_TRACE_ADDR_KEY, NULL);
	
	for (i = 0; i < G_N_ELEMENTS (helgrind_args); i++) {
		const char *arg = helgrind_args[i].arg;
		const char *key = helgrind_args[i].key;
		
		g_free (helgrind_args[i].buf);
		helgrind_args[i].buf = NULL;
		
		if (helgrind_args[i].depends && !enable_trace_addr)
			continue;
		
		if (helgrind_args[i].type == ARG_TYPE_STRING) {
			if (!(str = gconf_client_get_string (gconf, key, NULL)) || *str == '\0') {
				if (!strcmp (key, TRACE_ADDR_KEY))
					enable_trace_addr = FALSE;
				
				g_free (str);
				continue;
			}
			
			helgrind_args[i].buf = g_strdup_printf ("%s=%s", arg, str);
			g_free (str);
		} else if (helgrind_args[i].type == ARG_TYPE_INT) {
			val = gconf_client_get_int (gconf, key, NULL);
			helgrind_args[i].buf = g_strdup_printf ("%s=%d", arg, val);
		}
		
		g_ptr_array_add (argv, helgrind_args[i].buf);
	}
	
	g_object_unref (gconf);
}
