/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*  Alleyoop
 *  Copyright (C) 2003-2009 Jeffrey Stedfast
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licenses/>.
 */


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>

#include "vgerror.h"
#include "vgstrpool.h"


#define d(x) 
#define w(x) x

/* timestamp format used in Valgrind 2.4.0: "YY-MM-DD HH:MM:SS:MS" - based on date */
#define VG_2_4_0_TIME_STAMP_FORMAT "%u-%.2u-%.2u %.2u:%.2u:%.2u.%.3u"

/* timestamp format used in Valgrind 3.1.0: "DD:HH:MM:SS.MS" - based on time elapsed since program start */
#define VG_3_1_0_TIME_STAMP_FORMAT "%.2u:%.2u:%.2u:%.2u.%.3u"

enum {
	VG_ERROR_PARSER_STATE_INIT,
	VG_ERROR_PARSER_STATE_NEW_ERROR,
	VG_ERROR_PARSER_STATE_PARTIAL_ERROR,
	VG_ERROR_PARSER_STATE_WARNING = (1 << 8)
};

typedef struct _VgErrorListNode {
	struct _VgErrorListNode *next;
	struct _VgErrorListNode *prev;
	
	int state;
	
	pid_t pid;
	
	VgError *err_cur;
	
	VgErrorSummary *summ_cur;
	VgErrorSummary *summ_tail;
	
	VgErrorStack *stack_tail;
} VgErrorListNode;


VgErrorParser *
vg_error_parser_new (int fd, VgErrorCallback error_cb, void *user_data)
{
	VgErrorParser *parser;
	
	parser = g_new (VgErrorParser, 1);
	parser_init ((Parser *) parser, fd);
	
	parser->pid_hash = g_hash_table_new (g_direct_hash, g_direct_equal);
	list_init (&parser->errlist);
	
	parser->error_cb = error_cb;
	parser->user_data = user_data;
	
	return parser;
}


void
vg_error_parser_free (VgErrorParser *parser)
{
	VgErrorListNode *n;
	
	if (parser == NULL)
		return;
	
	g_hash_table_destroy (parser->pid_hash);
	
	while (!list_is_empty (&parser->errlist)) {
		n = (VgErrorListNode *) list_unlink_head (&parser->errlist);
		
		if (n->err_cur)
			vg_error_free (n->err_cur);
		
		g_free (n);
	}
	
	g_free (parser);
}


static int
vg_error_get_state (VgErrorParser *parser, pid_t pid)
{
	VgErrorListNode *n;
	
	if (!(n = g_hash_table_lookup (parser->pid_hash, GINT_TO_POINTER (pid))))
		return VG_ERROR_PARSER_STATE_INIT;
	
	return n->state;
}

static void
vg_error_set_state (VgErrorParser *parser, pid_t pid, int state)
{
	VgErrorListNode *n;
	
	if (!(n = g_hash_table_lookup (parser->pid_hash, GINT_TO_POINTER (pid)))) {
		d(fprintf (stderr, "VgErrorParser setting state on unknown pid??\n"));
		return;
	}
	
	n->state = state;
}

static VgError *
vg_error_new (VgErrorParser *parser, pid_t pid, timestamp_t *stamp)
{
	VgErrorListNode *n;
	VgError *err;
	
	if (!(n = g_hash_table_lookup (parser->pid_hash, GINT_TO_POINTER (pid)))) {
		n = g_new (VgErrorListNode, 1);
		n->pid = pid;
		
		list_append_node (&parser->errlist, (ListNode *) n);
		g_hash_table_insert (parser->pid_hash, GINT_TO_POINTER (pid), n);
	} else if (n->state == VG_ERROR_PARSER_STATE_NEW_ERROR) {
		return n->err_cur;
	}
	
	err = g_new (VgError, 1);
	memcpy (&err->stamp, stamp, sizeof (err->stamp));
	err->pid = pid;
	err->thread = (vgthread_t) -1;
	err->summary = NULL;
	
	n->err_cur = err;
	n->summ_cur = NULL;
	n->summ_tail = (VgErrorSummary *) &err->summary;
	n->stack_tail = NULL;
	n->state = VG_ERROR_PARSER_STATE_NEW_ERROR;
	
	return err;
}

static void
vg_error_pop (VgErrorParser *parser, pid_t pid)
{
	VgErrorListNode *n;
	
	if (!(n = g_hash_table_lookup (parser->pid_hash, GINT_TO_POINTER (pid))))
		return;
	
	if (n->err_cur) {
		if (n->err_cur->summary) {
			parser->error_cb (parser, n->err_cur, parser->user_data);
		} else {
			d(fprintf (stderr, "Incomplete valgrind error being popped??\n"));
			g_free (n->err_cur);
		}
	} else {
		d(fprintf (stderr, "VgErrorParser stack underflow??\n"));
	}
	
	n->state = VG_ERROR_PARSER_STATE_INIT;
	
	n->err_cur = NULL;
	n->summ_cur = NULL;
	n->summ_tail = NULL;
	n->stack_tail = NULL;
}

static void
vg_error_pop_all (VgErrorParser *parser)
{
	VgErrorListNode *n;
	
	n = (VgErrorListNode *) parser->errlist.head;
	while (n->next != NULL) {
		vg_error_pop (parser, n->pid);
		n = n->next;
	}
}

static void
vg_error_summary_append (VgErrorParser *parser, pid_t pid, const char *report, int len)
{
	VgErrorSummary *summary;
	VgErrorListNode *n;
	
	if (!(n = g_hash_table_lookup (parser->pid_hash, GINT_TO_POINTER (pid)))) {
		d(fprintf (stderr, "VgErrorParser appending summary to non-existant error report??\n"));
		return;
	}
	
	summary = g_new (VgErrorSummary, 1);
	summary->next = NULL;
	summary->parent = n->err_cur;
	summary->frames = NULL;
	summary->report = vg_strndup (report, len);
	
	n->summ_cur = summary;
	n->summ_tail->next = summary;
	n->summ_tail = summary;
	
	n->stack_tail = (VgErrorStack *) &summary->frames;
}

static VgErrorStack *
vg_error_stack_new (VgErrorParser *parser, pid_t pid)
{
	VgErrorStack *stack;
	VgErrorListNode *n;
	
	if (!(n = g_hash_table_lookup (parser->pid_hash, GINT_TO_POINTER (pid)))) {
		d(fprintf (stderr, "VgErrorParser appending stack frame to non-existant error report??\n"));
		return NULL;
	}
	
	stack = g_new (VgErrorStack, 1);
	stack->next = NULL;
	stack->summary = n->summ_cur;
	stack->where = VG_WHERE_AT;
	stack->addr = VG_STACK_ADDR_UNKNOWN;
	stack->type = VG_STACK_EMPTY;
	stack->symbol = NULL;
	stack->info.src.filename = NULL;
	stack->info.src.lineno = 0;
	stack->info.object = NULL;
	
	n->stack_tail->next = stack;
	n->stack_tail = stack;
	
	return stack;
}

static int
decode_timestamp_uint (unsigned char **in, size_t inlen, unsigned char expect,
		       int min, int max, const char *version, const char *token)
{
	register unsigned char *inptr = *in;
	unsigned char *inend = *in + inlen;
	int val = 0;
	
	while (inptr < inend && (*inptr >= '0' && *inptr <= '9')) {
		if (((val * 10) + (*inptr - '0')) < val) {
			val = -ERANGE;
			break;
		}
		
		val = (val * 10) + (*inptr - '0');
		inptr++;
	}
	
	if (inptr == *in) {
		w(fprintf (stderr, "Expected Valgrind %s timestamp %s token: '%.*s'\n", version,
			   token, inlen, *in));
		return -1;
	}
	
	if (!(val >= min && val <= max)) {
#if w(!)0
		if (val == -ERANGE) {
			while (*inptr >= '0' && *inptr <= '9')
				inptr++;
		}
		
		fprintf (stderr, "Value out of range for Valgrind %s timestamp %s token: '%.*s'\n",
			 version, token, inptr - *in, *in);
#endif
		return -1;
	}
	
	if (*inptr != expect) {
		w(fprintf (stderr, "Expected '%c' after Valgrind %s timestamp %s token: '%.*s'\n",
			   expect, version, token, inend - inptr, inptr));
		return -1;
	}
	
	*in = inptr;
	
	return val;
}

static unsigned int
decode_uint (unsigned char **in, size_t inlen)
{
	register unsigned char *inptr = *in;
	unsigned char *inend = *in + inlen;
	unsigned int val = 0;
	
	while (inptr < inend && (*inptr >= '0' && *inptr <= '9')) {
		if (((val * 10) + (*inptr - '0')) < val) {
			errno = ERANGE;
			return 0;
		}
		
		val = (val * 10) + (*inptr - '0');
		inptr++;
	}
	
	*in = inptr;
	errno = 0;
	
	return val;
}

static gboolean
decode_timestamp_and_pid (timestamp_t *stamp, pid_t *pid, unsigned char **in, size_t inlen)
{
	unsigned char *inend = *in + inlen;
	unsigned char *inptr = *in;
	unsigned char *start = *in;
	unsigned int uval;
	int val;
	
	/* extract a uint */
	uval = decode_uint (&inptr, inlen);
	if (inptr == *in) {
		w(fprintf (stderr, "Expected pid or timestamp token: '%.*s'\n", inend - inptr, inptr));
		return FALSE;
	}
	
	switch (*inptr) {
	case '-':
		/* Valgrind 2.4.0 timestamp */
		inptr = *in;
		stamp->format = TIMESTAMP_FORMAT_2_4_0;
		if ((val = decode_timestamp_uint (&inptr, (inend - inptr), '-', 0, INT_MAX, "2.4.0", "year")) == -1)
			return FALSE;
		
		stamp->ts_year = val;
		inptr++;
		
		if ((val = decode_timestamp_uint (&inptr, (inend - inptr), '-', 1, 12, "2.4.0", "month")) == -1)
			return FALSE;
		
		stamp->ts_month = val;
		inptr++;
		
		if ((val = decode_timestamp_uint (&inptr, (inend - inptr), ' ', 1, 31, "2.4.0", "day")) == -1)
			return FALSE;
		
		stamp->ts_day = val;
		inptr++;
		
		if ((val = decode_timestamp_uint (&inptr, (inend - inptr), ':', 0, 23, "2.4.0", "hour")) == -1)
			return FALSE;
		
		stamp->ts_hour = val;
		inptr++;
		
		if ((val = decode_timestamp_uint (&inptr, (inend - inptr), ':', 0, 59, "2.4.0", "minute")) == -1)
			return FALSE;
		
		stamp->ts_min = val;
		inptr++;
		
		if ((val = decode_timestamp_uint (&inptr, (inend - inptr), ':', 0, 59, "2.4.0", "second")) == -1)
			return FALSE;
		
		stamp->ts_sec = val;
		inptr++;
		
		if ((val = decode_timestamp_uint (&inptr, (inend - inptr), ' ', 0, 999, "2.4.0", "millisecond")) == -1)
			return FALSE;
		
		stamp->ts_msec = val;
		inptr++;
		break;
	case ':':
		/* Valgrind 3.1.0 timestamp */
		inptr = *in;
		stamp->format = TIMESTAMP_FORMAT_3_1_0;
		if ((val = decode_timestamp_uint (&inptr, (inend - inptr), ':', 0, INT_MAX, "3.1.0", "days")) == -1)
			return FALSE;
		
		stamp->ts_days = val;
		inptr++;
		
		if ((val = decode_timestamp_uint (&inptr, (inend - inptr), ':', 0, 23, "3.1.0", "hours")) == -1)
			return FALSE;
		
		stamp->ts_hours = val;
		inptr++;
		
		if ((val = decode_timestamp_uint (&inptr, (inend - inptr), ':', 0, 59, "3.1.0", "minutes")) == -1)
			return FALSE;
		
		stamp->ts_mins = val;
		inptr++;
		
		if ((val = decode_timestamp_uint (&inptr, (inend - inptr), '.', 0, 59, "3.1.0", "seconds")) == -1)
			return FALSE;
		
		stamp->ts_secs = val;
		inptr++;
		
		if ((val = decode_timestamp_uint (&inptr, (inend - inptr), ' ', 0, 999, "3.1.0", "milliseconds")) == -1)
			return FALSE;
		
		stamp->ts_msecs = val;
		inptr++;
		break;
	default:
		stamp->format = TIMESTAMP_FORMAT_NONE;
		goto pid;
	}
	
	start = inptr;
	uval = decode_uint (&inptr, inend - inptr);
	if (inptr == start) {
		w(fprintf (stderr, "Expected pid token: '%.*s'\n", inend - inptr, inptr));
		return FALSE;
	}
	
 pid:
	
	if (uval == 0) {
		w(fprintf (stderr, "Invalid pid received from valgrind: '%.*s'\n", inptr - start, start));
		return FALSE;
	}
	
	*pid = (pid_t) uval;
	
	*in = inptr;
	
	return TRUE;
}

int
vg_error_parser_step (VgErrorParser *parser)
{
	register unsigned char *inptr;
	unsigned char *start, *end;
	timestamp_t stamp;
	vgthread_t thread;
	int state, ret;
	VgError *err;
	Parser *priv;
	pid_t pid;
	
	priv = (Parser *) parser;
	
	if ((ret = parser_fill (priv)) == 0) {
		vg_error_pop_all (parser);
		return 0;
	} else if (ret == -1) {
		return -1;
	}
	
	start = inptr = priv->inptr;
	
	while (inptr < priv->inend) {
		*priv->inend = '\n';
		while (*inptr != '\n')
			inptr++;
		
		if (inptr == priv->inend)
			break;
		
		d(fprintf (stderr, "parser_step: '%.*s'\n", inptr - start, start));
		
		if (start[0] != '=' || start[1] != '=') {
			w(fprintf (stderr, "Unexpected data received from valgrind: '%.*s'\n", inptr - start, start));
			inptr++;
			start = inptr;
			continue;
		}
		
		start += 2;
		
		memset (&stamp, 0, sizeof (timestamp_t));
		if (!decode_timestamp_and_pid (&stamp, &pid, &start, inptr - start)) {
			inptr++;
			start = inptr;
			continue;
		}
		
		if (start[0] != '=' || start[1] != '=' || start[2] != ' ') {
			w(fprintf (stderr, "Unexpected data received from valgrind: '%.*s'\n", (inptr - start) + 2, start - 2));
			inptr++;
			start = inptr;
			continue;
		}
		
		start += 3;
		
		*inptr = '\0';
		if ((state = vg_error_get_state (parser, pid)) & VG_ERROR_PARSER_STATE_WARNING) {
			/* eat indenting */
			while (*start == ' ')
				start++;
			
			if (strcmp (start, "your program may misbehave as a result") == 0) {
				/* this marks the end of the Valgrind warning spewage */
				vg_error_set_state (parser, pid, state & ~VG_ERROR_PARSER_STATE_WARNING);
			}
			
			inptr++;
			start = inptr;
			continue;
		}
		
		if (state != VG_ERROR_PARSER_STATE_PARTIAL_ERROR) {
			/* brand new error - first line is the general report or thread-id (1.9.6 and later) */
			err = vg_error_new (parser, pid, &stamp);
			
			if (start[0] == ' ') {
				/* there should not be any additional error indenting here... */
				w(fprintf (stderr, "Unexpected SPACE received from valgrind: '%.*s'\n", inptr - start, start));
				
				while (*start == ' ')
					start++;
			}
			
			if (start < inptr) {
				if (strncmp (start, "discard syms in ", 16) == 0) {
					/* "discard syms in /path/to/lib/libfoo.so" */
					d(fprintf (stderr, "dropping 'discard syms in' spewage\n"));
				} else if (strstr (start, "IGNORED call to:") != NULL) {
					d(fprintf (stderr, "dropping ignored call notification\n"));
				} else if (strstr (start, "KLUDGED call to:") != NULL) {
					/* "valgrind's libpthread.so: KLUDGED call to: pthread_getschedparam" */
					d(fprintf (stderr, "dropping kludged call notification\n"));
				} else if (strncmp (start, "warning: ", 9) == 0) {
					/* "warning: Valgrind's pthread_getschedparam is incomplete" */
					d(fprintf (stderr, "dropping warning message\n"));
					vg_error_set_state (parser, pid, state | VG_ERROR_PARSER_STATE_WARNING);
				} else if (strncmp (start, "Thread ", 7) == 0) {
					start += 7;
					thread = strtoul (start, (char **) &end, 10);
					if (*end != ':') {
						start -= 7;
						vg_error_summary_append (parser, pid, start, inptr - start);
						vg_error_set_state (parser, pid, VG_ERROR_PARSER_STATE_PARTIAL_ERROR);
					} else {
						err->thread = thread;
					}
				} else {
					vg_error_summary_append (parser, pid, start, inptr - start);
					vg_error_set_state (parser, pid, VG_ERROR_PARSER_STATE_PARTIAL_ERROR);
				}
			}
		} else {
			/* another summary, a new stack frame, or end-of-info (ie. a blank line) */
			while (*start == ' ')
				start++;
			
			if (start < inptr) {
				if (!strncmp (start, "discard syms in ", 16)) {
					/* "discard syms in /path/to/lib/libfoo.so" */
					d(fprintf (stderr, "dropping 'discard syms in' spew received from Valgrind\n"));
				} else if (strstr (start, "IGNORED call to:") != NULL) {
					d(fprintf (stderr, "dropping ignored call notification\n"));
				} else if (strstr (start, "KLUDGED call to:") != NULL) {
					/* "valgrind's libpthread.so: KLUDGED call to: pthread_getschedparam" */
					d(fprintf (stderr, "dropping kludged call notification\n"));
				} else if (strncmp (start, "warning: ", 9) == 0) {
					/* "warning: Valgrind's pthread_getschedparam is incomplete" */
					d(fprintf (stderr, "dropping warning message\n"));
					vg_error_set_state (parser, pid, state | VG_ERROR_PARSER_STATE_WARNING);
				} else if (strncmp (start, "at ", 3) != 0 && strncmp (start, "by ", 3) != 0) {
					/* another summary report */
					vg_error_summary_append (parser, pid, start, inptr - start);
				} else {
					VgErrorStack *stack;
					
					stack = vg_error_stack_new (parser, pid);
					stack->where = *start == 'a' ? VG_WHERE_AT : VG_WHERE_BY;
					start += 3;
					
					if (*start == '<') {
						/* unknown address */
						stack->addr = VG_STACK_ADDR_UNKNOWN;
						
						while (start < inptr && *start != '>')
							start++;
						
						if (*start == '>')
							start++;
					} else {
						/* symbol address in hex */
						stack->addr = strtoul (start, (char **) &end, 16);
						start = end;
						
						if (*start == ':')
							start++;
					}
					
					if (*start == ' ')
						start++;
					
					if (!strncmp (start, "???", 3) && (start[3] == ' ' || start[3] == '\0')) {
						stack->symbol = NULL;
						start += 3;
					} else if (*start == '(') {
						/* not a c/c++ program, hence no symbol */
						stack->symbol = NULL;
					} else {
						/* symbol name */
						end = start;
						while (end < inptr && *end != ' ' && *end != '(')
							end++;
						
						if (*end == '(') {
							/* symbol name has a param list - probably a c++ symbol */
							while (end < inptr && *end != ')')
								end++;
							
							if (*end == ')')
								end++;
						}
						
						stack->symbol = vg_strndup (start, end - start);
						start = end;
					}
					
					if (*start == ' ')
						start++;
					
					if (*start == '(') {
						start++;
						
						/* if we have "([with]in foo)" then foo is an object... */
						if (strncmp (start, "within ", 7) == 0) {
							/* (within /usr/bin/emacs) */
							stack->type = VG_STACK_OBJECT;
							start += 7;
						} else if (strncmp (start, "in ", 3) == 0) {
							/* (in /lib/foo.so) */
							stack->type = VG_STACK_OBJECT;
							start += 3;
						} else {
							stack->type = VG_STACK_SOURCE;
						}
						
						end = start;
						while (end < inptr && *end != ':' && *end != ')')
							end++;
						
						/* src filename or shared object */
						if (stack->type == VG_STACK_SOURCE) {
							stack->info.src.filename = vg_strndup (start, end - start);
							
							start = end;
							if (*start++ == ':')
								stack->info.src.lineno = strtoul (start, (char **) &end, 10);
							else
								stack->info.src.lineno = 0;
						} else {
							stack->info.object = vg_strndup (start, end - start);
						}
						
						start = end;
					}
				}
			} else {
				/* end-of-info (ie. a blank line) */
				vg_error_pop (parser, pid);
			}
		}
		
		inptr++;
		start = inptr;
	}
	
	priv->inptr = start;
	
	return 1;
}


void
vg_error_parser_flush (VgErrorParser *parser)
{
	VgErrorListNode *n;
	
	n = (VgErrorListNode *) parser->errlist.head;
	while (n->next != NULL) {
		if (n->err_cur) {
			if (n->state == VG_ERROR_PARSER_STATE_PARTIAL_ERROR) {
				vg_error_pop (parser, n->pid);
			} else {
				g_free (n->err_cur);
				n->err_cur = NULL;
			}
		}
		
		n = n->next;
	}
}


static void
vg_error_stack_free (VgErrorStack *stack)
{
	vg_strfree (stack->symbol);
	if (stack->type == VG_STACK_SOURCE)
		vg_strfree (stack->info.src.filename);
	else
		vg_strfree (stack->info.object);
	
	g_free (stack);
}

static void
vg_error_summary_free (VgErrorSummary *summary)
{
	VgErrorStack *frame, *next;
	
	vg_strfree (summary->report);
	
	frame = summary->frames;
	while (frame != NULL) {
		next = frame->next;
		vg_error_stack_free (frame);
		frame = next;
	}
	
	g_free (summary);
}


void
vg_error_free (VgError *err)
{
	VgErrorSummary *summary, *next;
	
	if (err == NULL)
		return;
	
	summary = err->summary;
	while (summary != NULL) {
		next = summary->next;
		vg_error_summary_free (summary);
		summary = next;
	}
	
	g_free (err);
}

static void
vg_error_timestamp_to_string (const timestamp_t *stamp, GString *str)
{
	switch (stamp->format) {
	case TIMESTAMP_FORMAT_2_4_0:
		g_string_append_printf (str, VG_2_4_0_TIME_STAMP_FORMAT " ", stamp->ts_year,
					stamp->ts_month, stamp->ts_day, stamp->ts_hour,
					stamp->ts_min, stamp->ts_sec, stamp->ts_msec);
		break;
	case TIMESTAMP_FORMAT_3_1_0:
		g_string_append_printf (str, VG_3_1_0_TIME_STAMP_FORMAT " ", stamp->ts_days,
					stamp->ts_hours, stamp->ts_mins, stamp->ts_secs,
					stamp->ts_msecs);
		break;
	default:
		break;
	}
}

static void
vg_error_stack_to_string (VgErrorStack *stack, GString *str)
{
	timestamp_t *stamp = &stack->summary->parent->stamp;
	pid_t pid = stack->summary->parent->pid;
	int in;
	
	g_string_append (str, "==");
	vg_error_timestamp_to_string (stamp, str);
	g_string_append_printf (str, "%u==    %s ", pid, stack->where == VG_WHERE_AT ? "at" : "by");
	
	if (stack->addr != VG_STACK_ADDR_UNKNOWN)
		g_string_append_printf (str, "0x%.8x: ", stack->addr);
	else
		g_string_append (str, "<unknown address> ");
	
	g_string_append (str, stack->symbol ? stack->symbol : "???");
	
	switch (stack->type) {
	case VG_STACK_EMPTY:
		/* Stack generated by the Mono JIT (or similar)? */
		g_string_append_c (str, '\n');
		break;
	case VG_STACK_SOURCE:
		g_string_append_printf (str, " (%s:%u)\n", stack->info.src.filename, stack->info.src.lineno);
		break;
	case VG_STACK_OBJECT:
		in = !strcmp (stack->info.object + strlen (stack->info.object) - 3, ".so");
		in = in || strstr (stack->info.object, ".so.") != NULL;
		g_string_append_printf (str, " (%s %s)\n", in ? "in" : "within", stack->info.object);
		break;
	}
}

static void
vg_error_summary_to_string (VgErrorSummary *summary, int indent, GString *str)
{
	timestamp_t *stamp = &summary->parent->stamp;
	VgErrorStack *s;
	
	g_string_append (str, "==");
	vg_error_timestamp_to_string (stamp, str);
	g_string_append_printf (str, "%u== %s", summary->parent->pid, indent ? "   " : "");
	g_string_append (str, summary->report);
	g_string_append_c (str, '\n');
	
	s = summary->frames;
	while (s != NULL) {
		vg_error_stack_to_string (s, str);
		s = s->next;
	}
}


void
vg_error_to_string (VgError *err, GString *str)
{
	timestamp_t *stamp = &err->stamp;
	VgErrorSummary *s;
	int indent = 0;
	
	if (err->thread != (vgthread_t) -1) {
		g_string_append (str, "==");
		vg_error_timestamp_to_string (stamp, str);
		g_string_append_printf (str, "%u== Thread %d:\n", err->pid, err->thread);
	}
	
	s = err->summary;
	while (s != NULL) {
		vg_error_summary_to_string (s, indent, str);
		indent = indent || s->frames;
		s = s->next;
	}
	
	g_string_append (str, "==");
	vg_error_timestamp_to_string (stamp, str);
	g_string_append_printf (str, "%u==\n", err->pid);
}
