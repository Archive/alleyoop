/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*  Alleyoop
 *  Copyright (C) 2003-2009 Jeffrey Stedfast
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __LEGOLAS_H__
#define __LEGOLAS_H__

#include <sys/types.h>

#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus */

typedef size_t vma_t;

typedef struct _ElfDesc ElfDesc;

typedef struct _ElfSection {
	struct _ElfSection *next;
	struct _ElfSection *link;
	const char *name;
	ElfDesc *elfd;
	size_t index;
	void *data;
	vma_t vma;
} ElfSection;

typedef struct _ElfSymbol {
	struct _ElfSymbol *next;
	ElfSection *sect;
	const char *name;
	ElfDesc *elfd;
	size_t index;
} ElfSymbol;

typedef enum {
	ELF_TYPE_ANY,
	ELF_TYPE_RELOCATABLE,
	ELF_TYPE_EXECUTABLE,
	ELF_TYPE_SHARED_OBJECT,
	ELF_TYPE_CORE
} ElfType;

/* errno's */
#define ELF_ENOTELF      (-1)
#define ELF_EINVALCLASS  (-2)
#define ELF_EINVALENC    (-3)
#define ELF_EINVALVER    (-4)
#define ELF_EINVALOSABI  (-5)
#define ELF_EINVALTYPE   (-6)
#define ELF_ENOTTYPE     (-7)
#define ELF_ENOARCH      (-8)
#define ELF_ESHDRSIZE    (-9)


/* elf flags */
#define ELF_FLAGS_HAS_SYMS  (1 << 0)


/* ELF files */
ElfDesc *elf_open (const char *filename, ElfType expect);
void elf_close (ElfDesc *elfd);

const char *elf_get_filename (ElfDesc *elfd);

unsigned int elf_flags (ElfDesc *elfd);

void elf_dump (ElfDesc *elfd);

typedef void (* ElfSharedLibsForeachFunc) (ElfDesc *elfd, const char *libname, void *user_data);
void elf_shared_libs_foreach (ElfDesc *elfd, ElfSharedLibsForeachFunc cb, void *user_data);

/* ELF sections */
const ElfSection *elf_get_section_by_name (ElfDesc *elfd, const char *name);
#define elf_section_vma (section) (((ElfSection *) section)->vma)
size_t elf_section_get_size_before_reloc (const ElfSection *section);


/* ELF symbol tables */
ElfSymbol *elf_get_symbols (ElfDesc *elfd);
void elf_symbols_free (ElfSymbol *symbols);


/* ELF symbol lookups */
int elf_find_nearest_line (ElfDesc *elfd, const ElfSection *section, ElfSymbol **syms,
			   vma_t vma, const char **filename, const char **symname,
			   unsigned int *lineno);

int elf_symbols_lookup (ElfSymbol **syms, vma_t vma, const char **filename,
			const char **symname, unsigned int *lineno);


/* error strings */
const char *elf_strerror (int err);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __LEGOLAS_H__ */
