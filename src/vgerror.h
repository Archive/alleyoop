/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*  Alleyoop
 *  Copyright (C) 2003-2009 Jeffrey Stedfast
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __VG_ERROR_H__
#define __VG_ERROR_H__

#include <glib.h>
#include <stdint.h>
#include <time.h>

#include "list.h"
#include "parser.h"

#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus */

typedef enum {
	VG_WHERE_AT,
	VG_WHERE_BY,
} vgwhere_t;

typedef enum {
	VG_STACK_EMPTY,
	VG_STACK_SOURCE,
	VG_STACK_OBJECT,
} vgstack_t;

#define VG_STACK_ADDR_UNKNOWN ((unsigned int) -1)

typedef struct _VgError VgError;
typedef struct _VgErrorStack VgErrorStack;
typedef struct _VgErrorSummary VgErrorSummary;
typedef struct _VgErrorParser VgErrorParser;

struct _VgErrorStack {
	struct _VgErrorStack *next;       /* next stack frame */
	struct _VgErrorSummary *summary;  /* parent summary */
	vgwhere_t where;                  /* "at", "by" */
	unsigned int addr;                /* symbol address */
	vgstack_t type;                   /* func/obj */
	char *symbol;                     /* symbol name */
	union {
		struct {
			char *filename;
			size_t lineno;
		} src;
		char *object;
	} info;
};

struct _VgErrorSummary {
	struct _VgErrorSummary *next;
	struct _VgErrorStack *frames;
	struct _VgError *parent;
	char *report;
};

typedef unsigned long vgthread_t;

enum {
	TIMESTAMP_FORMAT_NONE,
	TIMESTAMP_FORMAT_2_4_0,
	TIMESTAMP_FORMAT_3_1_0,
};

typedef struct {
	uint32_t year;
	uint8_t month;
	uint8_t day;
	uint8_t hour;
	uint8_t min;
	uint8_t sec;
	uint16_t msec;
} timestamp24_t;

typedef struct {
	uint32_t days;
	uint8_t hours;
	uint8_t mins;
	uint8_t secs;
	uint16_t msecs;
} timestamp31_t;

typedef struct {
	uint8_t format;
	union {
		timestamp24_t ts24;
		timestamp31_t ts31;
	} ts;
} timestamp_t;

#define ts_year  ts.ts24.year
#define ts_month ts.ts24.month
#define ts_day   ts.ts24.day
#define ts_hour  ts.ts24.hour
#define ts_min   ts.ts24.min
#define ts_sec   ts.ts24.sec
#define ts_msec  ts.ts24.msec

#define ts_days  ts.ts31.days
#define ts_hours ts.ts31.hours
#define ts_mins  ts.ts31.mins
#define ts_secs  ts.ts31.secs
#define ts_msecs ts.ts31.msecs


struct _VgError {
	VgErrorSummary *summary;          /* first summary is the error, additional summary nodes are just more specifics */
	timestamp_t stamp;
	vgthread_t thread;
	pid_t pid;
};

typedef void (*VgErrorCallback) (VgErrorParser *parser, VgError *err, void *user_data);

struct _VgErrorParser {
	Parser parser;
	
	GHashTable *pid_hash;
	List errlist;
	
	VgErrorCallback error_cb;
	void *user_data;
};

VgErrorParser *vg_error_parser_new (int fd, VgErrorCallback error_cb, void *user_data);
void vg_error_parser_free (VgErrorParser *parser);

int vg_error_parser_step (VgErrorParser *parser);
void vg_error_parser_flush (VgErrorParser *parser);

void vg_error_free (VgError *err);

void vg_error_to_string (VgError *err, GString *str);

#ifdef _cplusplus
}
#endif /* __cplusplus */

#endif /* __VG_ERROR_H__ */
